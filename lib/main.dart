import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:showmax_prototype/search/presenter/SearchPresenter.dart';
import 'package:showmax_prototype/search/search.dart';

import 'ApiService.dart';

void main() {
  debugPaintSizeEnabled = false;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final ApiService _apiService = new ApiServiceImpl();
  SearchPresenter searchPresenter;

  MyApp() {
    searchPresenter = SearchPresenterImpl(_apiService);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Showmax',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        primaryColor: const Color(0xFFe91e63),
        accentColor: const Color(0xFFe91e63),
        canvasColor: const Color(0xFFfafafa),
      ),
      home: SearchPage(searchPresenter, title: 'Showmax'),
    );
  }
}
