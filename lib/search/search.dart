import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:showmax_prototype/api/model/image.dart' as showmaxImage;
import 'package:showmax_prototype/api/model/item.dart';
import 'package:showmax_prototype/search/presenter/SearchPresenter.dart';
import 'package:showmax_prototype/search/view/SearchView.dart';
import 'package:showmax_prototype/search/viewmodel/SearchViewModel.dart';
import 'package:showmax_prototype/util/hexcolor.dart';
import 'package:transparent_image/transparent_image.dart';

class SearchPage extends StatefulWidget {
  final SearchPresenter presenter;

  SearchPage(this.presenter, {Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> implements SearchView {
  SearchViewModel _searchViewModel;
  final textController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    debugPrint("initState");
    super.initState();
    this.widget.presenter.searchView = this; // assign view to presenter
  }

  @override
  void onNewViewModelReceived(SearchViewModel viewModel) {
    setState(() {
      this._searchViewModel = viewModel;
    });
  }

  @override
  void onShowToastMessage(String message) {
    setState(() {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message),
      ));
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Padding(
            padding: const EdgeInsets.all(16.0),
            child:
                Image.asset('assets/Showmax_white.png', fit: BoxFit.contain)),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: Padding(
                        padding: EdgeInsets.only(right: 16.0),
                        child: new TextField(
                          controller: textController,
                          onSubmitted: (s) => handleSearchQueryClick(),
                          decoration: InputDecoration(
                              hintText: "enter movie or tv series name"),
                        ))),
                RaisedButton(
                  onPressed: () => handleSearchQueryClick(),
                  child: Text("Search"),
                )
              ],
            ),
            createWidgetAccordingToState(context, _searchViewModel)
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  handleSearchQueryClick() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    this.widget.presenter.onSearchRequested(textController.text);
  }
}

Widget createWidgetAccordingToState(
    BuildContext context, SearchViewModel searchViewModel) {
  switch (searchViewModel.state) {
    case SearchViewState.SEARCH_IN_PROGRESS:
      return new Expanded(
          child: Center(child: CircularProgressIndicator(value: null)));
    case SearchViewState.SEARCH_RESULTS:
      return new Expanded(
          child: OrientationBuilder(builder: (context, orientation) {
        return GridView.count(
          crossAxisCount: orientation == Orientation.portrait ? 2 : 4,
          children: List.generate(searchViewModel.data.length, (index) {
            return constructGridItem(context, searchViewModel, index);
          }),
        );
      }));
    case SearchViewState.SEARCH_ERROR:
      return Center(child: new Text("Data loading error :-("));
  }
}

Widget constructGridItem(
    BuildContext context, SearchViewModel searchViewModel, int index) {
  final item = searchViewModel.data[index];
  final showmaxImage.Image image = Item.getPosterPortraitImage(item.images);
  String title = item.title;
  return Column(children: <Widget>[
    Expanded(
        child: Container(
      constraints: BoxConstraints.expand(),
      color: HexColor("#" + image.background_color),
      child: FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: image.link,
        fit: BoxFit.cover,
      ),
    )),
    Text(
      title,
      style: Theme.of(context).textTheme.body1,
      overflow: TextOverflow.ellipsis,
    )
  ]);
}
