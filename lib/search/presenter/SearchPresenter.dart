import 'package:flutter/foundation.dart';
import 'package:showmax_prototype/ApiService.dart';
import 'package:showmax_prototype/api/model/response.dart';
import 'package:showmax_prototype/search/view/SearchView.dart';
import 'package:showmax_prototype/search/viewmodel/SearchViewModel.dart';

abstract class SearchPresenter {
  void onSearchRequested(String query);

  set searchView(SearchView searchView);
}

class SearchPresenterImpl implements SearchPresenter {

  SearchPresenterImpl(this._apiService) {
    _searchViewModel = SearchViewModel();
  }

  ApiService _apiService;
  SearchView _searchView;
  SearchViewModel _searchViewModel;

  @override
  void onSearchRequested(String query) {
    debugPrint("onSearchRequested START $query");
    if (query.isEmpty) {
      _searchView.onShowToastMessage("Empty search query :-(");
      return;
    }
    _searchViewModel.state = SearchViewState.SEARCH_IN_PROGRESS;
    sendViewModelToView();
    _apiService
        .searchQuery(query)
        .then((ShowmaxResponse response) => onSearchRequestedFinished(response))
        .catchError((e) {
      handleError(e);
    });
  }

  @override
  void set searchView(SearchView searchView) {
    _searchView = searchView;
    sendViewModelToView();
  }

  onSearchRequestedFinished(ShowmaxResponse response) {
    debugPrint("onSearchRequestedFinished");
    _searchViewModel.data = response.items;
    _searchViewModel.state = SearchViewState.SEARCH_RESULTS;
    sendViewModelToView();
    final itemCount = _searchViewModel.data.length;
    _searchView.onShowToastMessage("$itemCount items found");
  }

  void handleError(e) {
    debugPrint("onSearchRequested ERROR");
    _searchViewModel.state = SearchViewState.SEARCH_ERROR;
    sendViewModelToView();
    _searchView.onShowToastMessage(e.error);
  }

  void sendViewModelToView() {
    if (_searchView == null) {
      debugPrint("searchView NULL !");
    }
    _searchView.onNewViewModelReceived(_searchViewModel);
  }

}
