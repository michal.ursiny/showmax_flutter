import 'package:showmax_prototype/api/model/item.dart';

enum SearchViewState {
   SEARCH_IN_PROGRESS,
   SEARCH_RESULTS,
   SEARCH_ERROR
}

class SearchViewModel {

  SearchViewState state = SearchViewState.SEARCH_RESULTS;
  List<Item> data = new List<Item>();

}
