import 'package:showmax_prototype/search/viewmodel/SearchViewModel.dart';

abstract class SearchView {

  void onNewViewModelReceived(SearchViewModel viewModel);
  void onShowToastMessage(String message);

}