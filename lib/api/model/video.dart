import 'package:json_annotation/json_annotation.dart';
import 'package:showmax_prototype/api/model/language.dart';

part 'video.g.dart';

@JsonSerializable()
class Video {

  Video(this.id, this.language, this.usage, this.ending_time, this.dar_frame,
      this.dar_image, this.duration, this.height, this.width, this.link);

  String id;
  Language language;
  String usage;
  double ending_time;
  double dar_frame;
  double dar_image;
  double duration;
  int height;
  int width;
  String link;

  factory Video.fromJson(Map<String, dynamic> json) => _$VideoFromJson(json);

  Map<String, dynamic> toJson() => _$VideoToJson(this);
}