// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crew.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Crew _$CrewFromJson(Map<String, dynamic> json) {
  return Crew(json['name'] as String, json['role'] as String);
}

Map<String, dynamic> _$CrewToJson(Crew instance) =>
    <String, dynamic>{'name': instance.name, 'role': instance.role};
