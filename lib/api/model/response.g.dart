// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShowmaxResponse _$ShowmaxResponseFromJson(Map<String, dynamic> json) {
  return ShowmaxResponse(
      (json['items'] as List)
          ?.map((e) =>
              e == null ? null : Item.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['count'] as int,
      json['remaining'] as int,
      json['total'] as int);
}

Map<String, dynamic> _$ShowmaxResponseToJson(ShowmaxResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
      'count': instance.count,
      'remaining': instance.remaining,
      'total': instance.total
    };
