import 'package:json_annotation/json_annotation.dart';
import 'package:showmax_prototype/api/model/language.dart';

part 'image.g.dart';

@JsonSerializable()
class Image {

  Image(this.id, this.link, this.type, this.orientation, this.aspect_ratio,
      this.language, this.background_color);

  String id;
  String link;
  String type;
  String orientation;
  double aspect_ratio;
  Language language;
  String background_color;

  factory Image.fromJson(Map<String, dynamic> json) => _$ImageFromJson(json);

  Map<String, dynamic> toJson() => _$ImageToJson(this);

}