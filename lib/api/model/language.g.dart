// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'language.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Language _$LanguageFromJson(Map<String, dynamic> json) {
  return Language(json['iso_639_3'] as String, json['name'] as String);
}

Map<String, dynamic> _$LanguageToJson(Language instance) =>
    <String, dynamic>{'iso_639_3': instance.iso_639_3, 'name': instance.name};
