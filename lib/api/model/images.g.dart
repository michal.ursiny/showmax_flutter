// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Images _$ImagesFromJson(Map<String, dynamic> json) {
  return Images(
      json['image'] as String,
      json['image_light'] as String,
      json['image_dark'] as String,
      json['image_dark_solid'] as String,
      json['image_light_solid'] as String);
}

Map<String, dynamic> _$ImagesToJson(Images instance) => <String, dynamic>{
      'image': instance.image,
      'image_light': instance.image_light,
      'image_dark': instance.image_dark,
      'image_dark_solid': instance.image_dark_solid,
      'image_light_solid': instance.image_light_solid
    };
