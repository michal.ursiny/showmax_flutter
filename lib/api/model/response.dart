import 'package:json_annotation/json_annotation.dart';
import 'package:showmax_prototype/api/model/item.dart';

/**
 * https://flutter.dev/docs/development/data-and-backend/json#code-generation
 */

part 'response.g.dart';

@JsonSerializable()
class ShowmaxResponse {

  ShowmaxResponse(this.items, this.count, this.remaining, this.total);

  List<Item> items;
  int count;
  int remaining;
  int total;

  factory ShowmaxResponse.fromJson(Map<String, dynamic> json) => _$ShowmaxResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ShowmaxResponseToJson(this);

}

