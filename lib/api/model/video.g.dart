// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Video _$VideoFromJson(Map<String, dynamic> json) {
  return Video(
      json['id'] as String,
      json['language'] == null
          ? null
          : Language.fromJson(json['language'] as Map<String, dynamic>),
      json['usage'] as String,
      (json['ending_time'] as num)?.toDouble(),
      (json['dar_frame'] as num)?.toDouble(),
      (json['dar_image'] as num)?.toDouble(),
      (json['duration'] as num)?.toDouble(),
      json['height'] as int,
      json['width'] as int,
      json['link'] as String);
}

Map<String, dynamic> _$VideoToJson(Video instance) => <String, dynamic>{
      'id': instance.id,
      'language': instance.language,
      'usage': instance.usage,
      'ending_time': instance.ending_time,
      'dar_frame': instance.dar_frame,
      'dar_image': instance.dar_image,
      'duration': instance.duration,
      'height': instance.height,
      'width': instance.width,
      'link': instance.link
    };
