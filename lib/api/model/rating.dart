import 'package:json_annotation/json_annotation.dart';
import 'package:showmax_prototype/api/model/images.dart';

part 'rating.g.dart';

@JsonSerializable()
class Rating {

  Rating(this.level, this.name, this.slug, this.images);

  String level;
  String name;
  String slug;
  Images images;

  factory Rating.fromJson(Map<String, dynamic> json) => _$RatingFromJson(json);

  Map<String, dynamic> toJson() => _$RatingToJson(this);
}

