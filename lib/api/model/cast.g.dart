// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cast.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cast _$CastFromJson(Map<String, dynamic> json) {
  return Cast(json['name'] as String);
}

Map<String, dynamic> _$CastToJson(Cast instance) =>
    <String, dynamic>{'name': instance.name};
