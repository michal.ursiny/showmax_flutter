// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) {
  return Item(
      json['id'] as String,
      json['provider'] as String,
      json['description'] as String,
      (json['crew'] as List)
          ?.map((e) =>
              e == null ? null : Crew.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['link'] as String,
      json['showmax_rating'] as String,
      json['title'] as String,
      (json['categories'] as List)
          ?.map((e) =>
              e == null ? null : Category.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['type'] as String,
      json['slug'] as String,
      (json['images'] as List)
          ?.map((e) =>
              e == null ? null : Image.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      (json['cast'] as List)
          ?.map((e) =>
              e == null ? null : Cast.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['section'] as String,
      (json['videos'] as List)
          ?.map((e) =>
              e == null ? null : Video.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['rating'] == null
          ? null
          : Rating.fromJson(json['rating'] as Map<String, dynamic>),
      json['year'] as int,
      json['has_download_policy'] as bool,
      (json['audio_languages'] as List)
          ?.map((e) =>
              e == null ? null : Language.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      (json['valid_subscription_statuses'] as List)
          ?.map((e) => e as String)
          ?.toList(),
      (json['subtitles_languages'] as List)
          ?.map((e) =>
              e == null ? null : Language.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['metadata_language'] == null
          ? null
          : Language.fromJson(
              json['metadata_language'] as Map<String, dynamic>));
}

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'id': instance.id,
      'provider': instance.provider,
      'description': instance.description,
      'crew': instance.crew,
      'link': instance.link,
      'showmax_rating': instance.showmax_rating,
      'title': instance.title,
      'categories': instance.categories,
      'type': instance.type,
      'slug': instance.slug,
      'images': instance.images,
      'cast': instance.cast,
      'section': instance.section,
      'videos': instance.videos,
      'rating': instance.rating,
      'year': instance.year,
      'has_download_policy': instance.has_download_policy,
      'audio_languages': instance.audio_languages,
      'valid_subscription_statuses': instance.valid_subscription_statuses,
      'subtitles_languages': instance.subtitles_languages,
      'metadata_language': instance.metadata_language
    };
