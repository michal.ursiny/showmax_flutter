import 'package:json_annotation/json_annotation.dart';

part 'images.g.dart';

@JsonSerializable()
class Images {

  Images(this.image, this.image_light, this.image_dark, this.image_dark_solid,
      this.image_light_solid);

  String image;
  String image_light;
  String image_dark;
  String image_dark_solid;
  String image_light_solid;

  factory Images.fromJson(Map<String, dynamic> json) => _$ImagesFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesToJson(this);

}