// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Image _$ImageFromJson(Map<String, dynamic> json) {
  return Image(
      json['id'] as String,
      json['link'] as String,
      json['type'] as String,
      json['orientation'] as String,
      (json['aspect_ratio'] as num)?.toDouble(),
      json['language'] == null
          ? null
          : Language.fromJson(json['language'] as Map<String, dynamic>),
      json['background_color'] as String);
}

Map<String, dynamic> _$ImageToJson(Image instance) => <String, dynamic>{
      'id': instance.id,
      'link': instance.link,
      'type': instance.type,
      'orientation': instance.orientation,
      'aspect_ratio': instance.aspect_ratio,
      'language': instance.language,
      'background_color': instance.background_color
    };
