import 'package:json_annotation/json_annotation.dart';
import 'package:showmax_prototype/api/model/cast.dart';
import 'package:showmax_prototype/api/model/category.dart';
import 'package:showmax_prototype/api/model/crew.dart';
import 'package:showmax_prototype/api/model/image.dart';
import 'package:showmax_prototype/api/model/language.dart';
import 'package:showmax_prototype/api/model/rating.dart';
import 'package:showmax_prototype/api/model/video.dart';

part 'item.g.dart';

@JsonSerializable()
class Item {
  Item(
      this.id,
      this.provider,
      this.description,
      this.crew,
      this.link,
      this.showmax_rating,
      this.title,
      this.categories,
      this.type,
      this.slug,
      this.images,
      this.cast,
      this.section,
      this.videos,
      this.rating,
      this.year,
      this.has_download_policy,
      this.audio_languages,
      this.valid_subscription_statuses,
      this.subtitles_languages,
      this.metadata_language);

  String id;
  String provider;
  String description;
  List<Crew> crew;
  String link;
  String showmax_rating;
  String title;
  List<Category> categories;
  String type;
  String slug;
  List<Image> images;
  List<Cast> cast;
  String section;
  List<Video> videos;
  Rating rating;
  int year;
  bool has_download_policy;
  List<Language> audio_languages;
  List<String> valid_subscription_statuses;
  List<Language> subtitles_languages;
  Language metadata_language;

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  Map<String, dynamic> toJson() => _$ItemToJson(this);

  static Image getPosterPortraitImage(List<Image> images) {
    if ((images == null) || (images.length == 0)) {
      return null;
    }
    Image result = images[0]; // default fallback
    for (var value in images) {
      if ((value.type == "poster") && (value.orientation == "square")) {
        return value;
      }
    }
    return result;
  }

}
