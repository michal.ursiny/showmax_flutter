// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Rating _$RatingFromJson(Map<String, dynamic> json) {
  return Rating(
      json['level'] as String,
      json['name'] as String,
      json['slug'] as String,
      json['images'] == null
          ? null
          : Images.fromJson(json['images'] as Map<String, dynamic>));
}

Map<String, dynamic> _$RatingToJson(Rating instance) => <String, dynamic>{
      'level': instance.level,
      'name': instance.name,
      'slug': instance.slug,
      'images': instance.images
    };
