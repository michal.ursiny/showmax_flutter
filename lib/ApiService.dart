import 'package:showmax_prototype/api/model/response.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

abstract class ApiService {

   Future<ShowmaxResponse> searchQuery(String query);

}

class ApiServiceImpl implements ApiService {

  String _base_url = "api.showmax.io";
  String _api_version = "116.0";
  String _api_platform = "android";

  @override
  Future<ShowmaxResponse> searchQuery(String query) async {
     final response =
     await http.get('https://$_base_url/v$_api_version/$_api_platform/catalogue/search?q=$query');

     if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON
        return ShowmaxResponse.fromJson(json.decode(response.body));
     } else {
        // If that call was not successful, throw an error.
        throw Exception('Failed to load response');
     }
  }

}